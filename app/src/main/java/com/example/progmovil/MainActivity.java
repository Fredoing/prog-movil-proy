package com.example.progmovil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.firebase.ui.database.FirebaseRecyclerOptions;

public class MainActivity extends AppCompatActivity {

    public static final String IND = "INDICE";

    private DatoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Solicitar los datos en la base y cargarlos a el recycler
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        Firebase mfirebase = Firebase.getInstance();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        FirebaseRecyclerOptions<Dato> options =
                new FirebaseRecyclerOptions.Builder<Dato>()
                        .setQuery(mfirebase.getmDatabase().child("Prueba"), Dato.class).build();

        adapter = new DatoAdapter(options);
        recyclerView.setAdapter(adapter);

        // listener para el boton agregar
        Button openAgregar = (Button) findViewById(R.id.btnAgregar);
        openAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAgregar();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public void startAgregar() {
        Intent intent = new Intent(this, AgregarActivity.class);
        intent.putExtra(IND, adapter.getItemCount());
        startActivity(intent);
    }
}