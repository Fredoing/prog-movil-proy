package com.example.progmovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class EditarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);

        Firebase mfirebase = Firebase.getInstance();

        Intent intent = getIntent();
        String sNombre = intent.getStringExtra(DatoAdapter.NOMBRE);
        String sDetalle = intent.getStringExtra(DatoAdapter.DETALLE);
        int pos = intent.getIntExtra(DatoAdapter.POS, 1);

        TextView nombre = findViewById(R.id.editNombre);
        TextView detalle = findViewById(R.id.editDetalle);
        nombre.setText(sNombre);
        detalle.setText(sDetalle);

        Button editar = findViewById(R.id.btnEditar);
        Button eliminar = findViewById(R.id.btnEliminar);

        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombreS = nombre.getText().toString();
                String detalleS = detalle.getText().toString();
                mfirebase.updateDB(pos, nombreS, detalleS);
                finish();
            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mfirebase.removeDB(pos);
                finish();
            }
        });

    }
}