package com.example.progmovil;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Iterator;

/*
    Clase singleton que se encarga de las operaciones con la base de datos
 */
public class Firebase {
    private DatabaseReference mDatabase;
    private static Firebase single_firebase = null;
    private ArrayList<Dato> datos;

    private Firebase(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        datos = new ArrayList<Dato>();
        loadLocal();
    }

    public static Firebase getInstance() {
        if (single_firebase == null) {
            single_firebase = new Firebase();
        }
        return single_firebase;
    }

    public DatabaseReference getmDatabase() {
        return mDatabase;
    }

    // escribri los datos en la base de datos
    public void writeToDB(String nombre, String detalle) {
        int id = insertLocal(nombre, detalle);
        Dato dato = new Dato(id, nombre, detalle);
        String ind = "datos".concat(String.valueOf(id));
        mDatabase.child("Prueba").child(ind).setValue(dato);
    }

    // actualizar datos de un campo
    public void updateDB(int id, String nombre, String detalle){
        Dato dato = new Dato(id, nombre, detalle);
        String ind = "datos".concat(String.valueOf(id));
        mDatabase.child("Prueba").child(ind).setValue(dato);
    }

    // eliminar un campo especifico
    public void removeDB(int id){
        String ind = "datos".concat(String.valueOf(id));
        mDatabase.child("Prueba").child(ind).removeValue();
    }

    // manejo de datos de forma local para evitar inconsistencias
    public int insertLocal(String nombre, String detalle){
        Dato dato;
        int ind;
        if (datos.isEmpty()){
            ind = 1;
        } else {
            ind = datos.get(datos.size()-1).id;
            ind++;
        }
        dato = new Dato(ind, nombre, detalle);
        datos.add(dato);
        return ind;
    }

    // cargar los datos de la base de datos
    // se utiliza una sola vez cuando inicia la aplicacion
    public void loadLocal() {
        mDatabase.child("Prueba").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                Iterable<DataSnapshot> iterable = task.getResult().getChildren();
                Iterator<DataSnapshot> iterator = iterable.iterator();
                while (iterator.hasNext()){
                    DataSnapshot dataSnapshot = iterator.next();

                    int id = Integer.parseInt(dataSnapshot.child("id").getValue().toString());
                    String nombre =  dataSnapshot.child("nombre").getValue().toString();
                    String detalle = dataSnapshot.child("detalle").getValue().toString();
                    Dato dato = new Dato(id, nombre, detalle);
                    datos.add(dato);
                }
            }
        });
    }

}
