package com.example.progmovil;


import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

public class DatoAdapter extends FirebaseRecyclerAdapter<Dato, DatoAdapter.DatoViewholder> {

    public static final String NOMBRE = "NOMBRE";
    public static final String DETALLE = "DETALLE";
    public static final String POS = "POSITION";

    public DatoAdapter(
            @NonNull FirebaseRecyclerOptions<Dato> options)
    {
        super(options);
    }

    // Se cargan los datos de la base al recycler
    @Override
    protected void onBindViewHolder(@NonNull DatoViewholder holder,
                                    int position, @NonNull Dato model){
        holder.detalle.setText(model.detalle);
        holder.nombre.setText(model.nombre);
        // listener para poder abrir una activity nueva con los datos que muestra
        holder.mview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), EditarActivity.class);
                intent.putExtra(NOMBRE, model.nombre);
                intent.putExtra(DETALLE, model.detalle);
                intent.putExtra(POS, model.id);
                v.getContext().startActivity(intent);
            }
        });
    }

    // asignar un layout para mostrar los datos en el recycler
    @NonNull
    @Override
    public DatoViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewtype){
        View view =
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layoutdato, parent, false);
        return new DatoAdapter.DatoViewholder(view);
    }

    // subclase utilizada para determinar los views del elemento
    // que muestra los datos
    class DatoViewholder extends RecyclerView.ViewHolder {
        TextView nombre, detalle;
        View mview;
        public DatoViewholder(@NonNull View itemview) {
            super(itemview);
            nombre = itemview.findViewById(R.id.txtNombre);
            detalle = itemview.findViewById(R.id.txtDetalle);
            mview = itemview;
        }
    }


}
