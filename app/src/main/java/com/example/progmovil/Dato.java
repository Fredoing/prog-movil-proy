package com.example.progmovil;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Dato {
    public String nombre;
    public String detalle;
    public int id;

    public Dato() {

    }

    public Dato(int new_id, String new_nombre, String new_detalle) {
        id = new_id;
        nombre = new_nombre;
        detalle = new_detalle;
    }
}
