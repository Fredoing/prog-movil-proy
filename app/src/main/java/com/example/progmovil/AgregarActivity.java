package com.example.progmovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AgregarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar);

        Intent intent = getIntent();
        int pos = intent.getIntExtra(MainActivity.IND, 1);

        TextView txtNombre = (TextView) findViewById(R.id.TextNombre);
        TextView txtDetalle = (TextView) findViewById(R.id.TextDetalle);
        Button btnAgregar = (Button) findViewById(R.id.btnInsertar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Firebase mfirebase = Firebase.getInstance();
                String nombre = txtNombre.getText().toString();
                String detalle = txtDetalle.getText().toString();
                if (!nombre.isEmpty() && !detalle.isEmpty()){
                    mfirebase.writeToDB(nombre, detalle);
                }
                finish();
            }
        });
    }
}